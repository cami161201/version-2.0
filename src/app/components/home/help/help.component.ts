import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { combineLatest, startWith, map } from 'rxjs';
import { ProfileUser } from 'src/app/models/user';
import { ChatsService } from 'src/app/services/chats.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css'],
})
export class HelpComponent implements OnInit {
  user$ = this._usersService.currentUserProfile$;

  searchControl = new FormControl('');
  chatListControl = new FormControl('');

  users$ = combineLatest([
    this._usersService.AllUsers$,
    this.user$,
    this.searchControl.valueChanges.pipe(startWith('')),
  ]).pipe(
    map(([users, user, searchString]) =>
      users.filter(
        (u) =>
          u.displayName?.toLowerCase().includes(searchString.toLowerCase()) &&
          u.uid !== user?.uid
      )
    )
  );

  constructor(
    private _usersService: UsersService,
    private chatsService: ChatsService
  ) {}

  myChats$ = this.chatsService.myChats$;

  selectedChat$ = combineLatest([
    this.chatListControl.valueChanges,
    this.myChats$,
  ]).pipe(map(([value, chats]) => chats.find((c) => c.id === value[0])));
  ngOnInit(): void {}

  createChat(otherUser: ProfileUser) {
    this.chatsService.createChet(otherUser).subscribe();
  }
}
