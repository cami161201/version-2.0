import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IComentario } from 'src/app/models/comentario';
import { UsersService } from 'src/app/services/users.service';
import { ExtrasService } from 'src/app/services/extras.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatCard } from '@angular/material/card';
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
})
export class InicioComponent implements OnInit {
  com:IComentario[]=[];
  panelOpenState = false;
  form!: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private comenService: ExtrasService,
    private _snackbar:MatSnackBar
  ) {}

  ngOnInit(): void {
    this.com= this.comenService.obtenerComentario()
  }
  primeroFormGroup = this._formBuilder.group({
    primeroCtrl: ['', Validators.required],
  });
  segundoFormGroup = this._formBuilder.group({
    segundoCtrl: ['', Validators.required],
  });
  terceroFormGroup = this._formBuilder.group({
    tercerCtrl: ['', Validators.required],
  });
  cuartoFormGroup = this._formBuilder.group({
    cuartoCtrl: ['', Validators.required],
  });
  quintoFormGroup = this._formBuilder.group({
    quintoCtrl: ['', Validators.required],
  });
COMENTARIOS:IComentario[] = [];
datos!:MatCard;

cargarComentario(){
this.COMENTARIOS = this.comenService.obtenerComentario();
this.datos=new MatCard;
}

  agregarComentarios(comentario: HTMLInputElement) {
    this.comenService.agregarComentarios({
      mensaje: comentario.value,
    });
    console.log(this.comenService.obtenerComentario());

    return false;
  }

  borrarComentario(comentario:IComentario){
    const opcion = confirm(`¿Esta seguro de eliminar el comentario?`);
    if (opcion) {
      console.log(comentario);
      this.comenService.eliminarComentario(comentario);
      this.cargarComentario();
      this._snackbar.open("La cita fue eliminada con exito", "Aceptar", {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      })
    }
 
  }



 
}


