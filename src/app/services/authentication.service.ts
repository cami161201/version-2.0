import { Injectable } from '@angular/core';
import { Auth, authState, createUserWithEmailAndPassword, updateProfile, UserInfo } from '@angular/fire/auth';
import { signInWithEmailAndPassword } from '@firebase/auth';
import { concatMap, from, Observable, of, switchMap } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  currentUser$ = authState(this.auth);

  constructor(private auth : Auth) { }

  // Para subir a firebase
  login(email:string,password:string):Observable<any>{
   return from(signInWithEmailAndPassword(this.auth,email,password));
  }
  updateProfile(profileData: Partial<UserInfo>): Observable<any> {
      const user = this.auth.currentUser;
      return of(user).pipe(
        concatMap((user) => {
          if (!user) throw new Error('Not authenticated');
  
          return updateProfile(user, profileData);
        })
      );
    }
  signUp(email: string, password: string) {
    return from(createUserWithEmailAndPassword(this.auth, email, password));
  }
  logout(){
    return from(this.auth.signOut());
  }
}
